package Bridge;

import java.util.Random;

import junit.framework.TestCase;

public class  Bridge_con extends TestCase {
	private static int inicio = 0;
	private static int repeticiones = 1000000;
	private static int prueba;
	
	public Bridge_con(String sTestName)
	{
		super(sTestName);
	}

	public void setUp() throws Exception
	{
	}

	public void tearDown() throws Exception
	{
	}

	private void test() throws InterruptedException
	{
		
		
		System.out.println("inicio del test: abstract factory, p"+prueba+" ["+inicio+","+(inicio+repeticiones)+"]");
		for(int i=10 ;i>-1 ; i--) {
			Thread.sleep(1000);
			System.out.println("en: "+i+" seg");
		}
		

		for(int i=inicio; i< inicio+repeticiones ;i++) {
			System.out.println(i);
			
			StackArray[] stacks = {new StackArray(), new StackFIFO(), new StackHanoi()};
	        StackList stackList = new StackList();
	        for (int j = 1, num; j < 15; j++) {
	            stacks[0].push(j);
	            stackList.push(j);
	            stacks[1].push(j);
	        }
	        Random rn = new Random();
	        for (int j = 1, num; j < 15; j++) {
	            stacks[2].push(rn.nextInt(20));
	        }
	        while (!stacks[0].isEmpty()) {
	        	stacks[0].pop();
	        }
	        System.out.println();
	        while (!stackList.isEmpty()) {
	        	stackList.pop();
	        }
	        System.out.println();
	        while (!stacks[1].isEmpty()) {
	        	stacks[1].pop();
	        }
	        System.out.println();
	        while (!stacks[2].isEmpty()) {
	        	stacks[2].pop();
	        }
	        System.out.println();
	        ((StackHanoi) stacks[2]).reportRejected();
		}
				
		System.out.println("fin del test: abstract factory, p"+prueba+" ["+inicio+","+(inicio+repeticiones)+"]");
	}
	
	public void testNuevoAdmin() throws InterruptedException
	{
		for(prueba = 1; prueba<21 ;prueba++) {
			test();
			inicio += repeticiones;
		}
	}
	

}
