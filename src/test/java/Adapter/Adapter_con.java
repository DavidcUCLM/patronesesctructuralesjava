package Adapter;


import junit.framework.TestCase;

public class Adapter_con extends TestCase {
	private static int inicio = 0;
	private static int repeticiones = 1000000;
	private static int prueba;
	
	public Adapter_con(String sTestName)
	{
		super(sTestName);
	}

	public void setUp() throws Exception
	{
	}

	public void tearDown() throws Exception
	{
	}

	private void test() throws InterruptedException
	{
		
		
		System.out.println("inicio del test: abstract factory, p"+prueba+" ["+inicio+","+(inicio+repeticiones)+"]");
		for(int i=10 ;i>-1 ; i--) {
			Thread.sleep(1000);
			System.out.println("en: "+i+" seg");
		}
		

		for(int i=inicio; i< inicio+repeticiones ;i++) {
			System.out.println(i);
			
			RoundHole roundHole = new RoundHole( 5 );
	        SquarePegAdapter squarePegAdapter;
	        for (int j = 6; j < 10; j++) {
	            squarePegAdapter = new SquarePegAdapter((double)j);
	            // The client uses (is coupled to) the new interface
	            squarePegAdapter.makeFit(roundHole);
	        }
		}
				
		System.out.println("fin del test: abstract factory, p"+prueba+" ["+inicio+","+(inicio+repeticiones)+"]");
	}
	
	public void testNuevoAdmin() throws InterruptedException
	{
		for(prueba = 1; prueba<21 ;prueba++) {
			test();
			inicio += repeticiones;
		}
	}
	

}
